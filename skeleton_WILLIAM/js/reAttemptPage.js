// Code for the Measure Run page.

//Variables
let marker = 0, road = 0, timing, saveLoc;

let reAttemptIndex = JSON.parse(localStorage.getItem('ReAttemptIndex'));

let startLocation = savedRuns[reAttemptIndex].startLocation;
let startLong = savedRuns[reAttemptIndex].startLocation[0];
let startLat = savedRuns[reAttemptIndex].startLocation[1];
let endLocation = savedRuns[reAttemptIndex].destination;




document.getElementById("stop").disabled = true;
document.getElementById("start").disabled = false;
document.getElementById("save").disabled = true;

mapboxgl.accessToken = 'pk.eyJ1IjoibmljaG9sYXNuaWNob2xhcyIsImEiOiJjanpyd2hoZ3MxNm9hM25sbTkxc2Z3M3c0In0.0IOC3AapHLE9kYIBGwSbDQ';


let running = new Run(startLong,startLat, endLocation);
running.startLocation = startLocation;

let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    zoom: 16.5,
    center: startLocation

});
// The button for the map to zoom in or out.
map.addControl(new mapboxgl.NavigationControl());

// The button for the tracking of the current button.
map.addControl(new mapboxgl.GeolocateControl({
    positionOptions: {
        enableHighAccuracy: true,
        maxZoom: 20
    },
    trackUserLocation: true
}));

// Marker properties
let marker1 = new mapboxgl.Marker({
    color: "red"
});
let marker2 = new mapboxgl.Marker({
    color: "blue"
});

//show history marker
marker1.setLngLat([startLocation[0],startLocation[1]]);
marker1.addTo(map);

marker2.setLngLat([endLocation[0],endLocation[1]]);
marker2.addTo(map);

//show path
map.on('load', function () {

    map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": [
                        endLocation,
                        startLocation
                    ]
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#888",
            "line-width": 6
        }
    });
});




// Function for buttons
// To get the current location.
function buttonGetLocation() {
    alert(running.getLocation());
}


// Start time.
function startTimer() {
    running.runStartTime = new Date().getTime();
    timing = setInterval(counter, 1000);
    saveLoc = setInterval(saveLocation, 5000);
    document.getElementById("start").disabled = true;
    document.getElementById("stop").disabled = false;

}

// Purpose is to stop the timer.
function stopTimer() {
    clearTimeout(timing);
    clearTimeout(saveLoc);
    document.getElementById("stop").disabled = true;
    document.getElementById("save").disabled = false;


}

// To calculate the duration of the run.
function counter() {
    running.runFinishTime= new Date().getTime();
    running.runDuration = Math.floor((running.runFinishTime - running.runStartTime) / 1000) ;
    //display time to html
    document.getElementById("time").innerHTML = running.runDuration + " Seconds";
}

function saveLocation() {
    let loc = JSON.parse(localStorage.getItem("current"));
    running.runTravelLocations.push(loc);
}


function saveRun() {

    let ask = prompt("Please enter the name of this run");

    while (ask === "") {
        ask = prompt("Please enter the name of this run");
    }

    running.name = ask;
    savedRuns.push(running);
    localStorage.setItem("saveRuns", JSON.stringify(savedRuns));


    document.getElementById("save").disabled = true;
    alert('You have successfully save the run');
    location.href = 'index.html';

}


