// Code for the View Run page.

let initLocation = JSON.parse(localStorage.getItem("current"));
let runningIndex = JSON.parse(localStorage.getItem("runIndex"));
let travelLocations = savedRuns[runningIndex].runTravelLocations;
document.getElementById("headerBarTitle").innerHTML = savedRuns[runningIndex].name;



mapboxgl.accessToken = 'pk.eyJ1Ijoid2lsbGlhbWxlZTE5IiwiYSI6ImNqeWpmdW1pZzAydngzbm81eWx2M2dqbHMifQ.LdhMMbIXTjGwBqxDlLOQqg';
let map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
    center: savedRuns[runningIndex].startLocation, // starting position [lng, lat]
    zoom: 16.5 // starting zoom
});

// The button for the map to zoom in or out.
map.addControl(new mapboxgl.NavigationControl());

// The button for the tracking of the current button.
map.addControl(new mapboxgl.GeolocateControl({
    positionOptions: {
        enableHighAccuracy: true,
        maxZoom: 20
    },
    trackUserLocation: true
}));

openRun(runningIndex);

function openRun(index) {
    let cords = travelLocations;
    cords.unshift(savedRuns[index].startLocation);
    //console.log(typeof (cords));
    //showPath(cords[1],cords[2]);

    map.on('load', function () {

        map.addLayer({
            "id": "route",
            "type": "line",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [
                            savedRuns[index].destination,
                            savedRuns[index].startLocation
                        ]
                    }
                }
            },
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#888",
                "line-width": 7
            }
        });
    });

    for (let k = 0; k < cords.length -1; k++) {
        showPath(cords[k],cords[k + 1], k.toString());
    }

    showMarker(index);
    showDetails(index);
}

function showMarker(index) {
    let marker1 = new mapboxgl.Marker({
        color: "red"
    });
    let marker2 = new mapboxgl.Marker({
        color: "blue"
    });
    marker1.setLngLat(savedRuns[index].startLocation);
    marker1.addTo(map);
    marker2.setLngLat(savedRuns[index].destination);
    marker2.addTo(map);
}

function showPath(loc1, loc2, id) {
    map.on('load', function () {

        map.addLayer({
            "id": id,
            "type": "line",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [
                            loc1, loc2
                        ]
                    }
                }
            },
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#308dff",
                "line-width": 5
            }
        });
    });
}

function showDetails(index) {
    let word = "The total distance traveled is " + calCulateTotalRunDistance(index) +
        " meter, the average speed is " + (calCulateTotalRunDistance(index) / savedRuns[index].runDuration).toFixed(3) +
         "m/s, the duration is " + savedRuns[index].runDuration + " seconds";

    document.getElementById("details").innerHTML = word;

}

function calCulateTotalRunDistance(index) {

    let totalDistance = 0;
    let cords = savedRuns[index].runTravelLocations;
    cords.unshift(savedRuns[index].startLocation);

    for (let j = 0; j < savedRuns[index].runTravelLocations.length -1; j++)
    {
        // The radius of earth in metres.
        let earthRad = 6371e3;
        // Intial Latitude and final latitude.
        let startLatitude = toRadians(cords[j][1]);
        let endLatitude = toRadians(cords[j + 1][1]);

        // Initial Longitude and final longitude.
        let startLongitude = toRadians(cords[j][0]);
        let endLongitude = toRadians(cords[j + 1][0]);

        // The difference of the latitudes and longitudes.
        let latDif = toRadians(cords[j + 1][1] - cords[j][1]);
        let longDif = toRadians(cords[j + 1][0] - cords[j][0]);

        // The calculation of Haversine formula.
        let haversine = Math.sin(latDif/2) * Math.sin(latDif/2) + Math.cos(startLatitude) * Math.cos(endLongitude) * Math.sin(longDif/2) * Math.sin(longDif/2);
        let formula = 2 * Math.atan2(Math.sqrt(haversine), Math.sqrt(1-haversine));

        // The distance between the two location.
        let distance = earthRad * formula;
        //console.log (typeof (distance));
        console.log(distance);
        if ( isNaN(distance)){
            distance = 0;
        }
        else {
            totalDistance += distance;

        }
    }
    return totalDistance.toFixed(3);
}
function deleteRun() {
    let newList = savedRuns;
    newList.splice(runningIndex, 1);
    console.log(newList);
    localStorage.setItem("saveRuns", JSON.stringify(newList));
    alert('You have successfully delete the run');
    location.href = 'index.html';
}

function reattemptRun() {
    localStorage.setItem("ReAttemptIndex", JSON.stringify(runningIndex));
    location.href = 'reAttempt.html';
}


