// Shared code needed by all three pages.
let long, lat;

// Array of saved Run objects.
let savedRuns = JSON.parse(localStorage.getItem('saveRuns')) || [];

setInterval(watch, 5000);

function watch() {
    navigator.geolocation.watchPosition(function(position) {
        locate(position.coords.longitude, position.coords.latitude);
    });
}


// Function for the above navigator to access the longitude and latitude.
function locate(a, b) {
    long = a;
    lat = b;
    localStorage.setItem("current", JSON.stringify([long,lat]));
}

// Function for converting degree to radian.
function toRadians(value) {
    let degrees = value;
    return degrees * (Math.PI/180);
}

// Class for runner app.
class Run {


    constructor(newLong, newLat, newDestination) {
        this.longitude = newLong;
        this.latitude = newLat;
        this.error = 0;
        this.distance = 0;
        this.startLocation = [];
        this.destination = newDestination;
        this.name ="";
        this.runStartTime = 0;
        this.runFinishTime = 0;
        this.runDuration = 0;
        this.runTravelLocations = [];

    }

    getLocation() {
        let loc = JSON.parse(localStorage.getItem("current"));
        return ("Current Long = " + loc[0] + " Current Lat = " + loc[1]);
    }



    setRandomLocation() {
        // Using the pre-written random value generator
        //let coordinates = new Run(this.longitude, this.latitude);
        let randomDestination = this.randomGen();

        // LOCAL STORAGE, KEY = randomDestination, THE RANDOM LOCATION GENERATED
        localStorage.setItem("randomDestination",JSON.stringify(randomDestination));

        // The radius of earth in metres.
        let earthRad = 6371e3;

        // Intial Latitude and final latitude.
        let startLatitude = toRadians(this.latitude);
        let endLatitude = toRadians(randomDestination[1]);

        // Initial Longitude and final longitude.
        let startLongitude = toRadians(this.longitude);
        let endLongitude = toRadians(randomDestination[0]);

        // The difference of the latitudes and longitudes.
        let latDif = toRadians(randomDestination[1] - this.latitude);
        let longDif = toRadians(randomDestination[0] - this.longitude);

        // The calculation of Haversine formula.
        let haversine = Math.sin(latDif/2) * Math.sin(latDif/2) + Math.cos(startLatitude) * Math.cos(endLongitude) * Math.sin(longDif/2) * Math.sin(longDif/2);
        let formula = 2 * Math.atan2(Math.sqrt(haversine), Math.sqrt(1-haversine));

        // The distance between the two location.
        this.distance = earthRad * formula;

        // Checking the distance is around 60m to 150m.
        if (this.distance > 60 && this.distance < 150) {
            this.destination = randomDestination;
            alert(`New location is ${randomDestination}`); // For testing only
            // Setting a marker
            let roundedDistance = Math.floor(this.distance)
            console.log(roundedDistance);
            /*
            if (check === 0) {
                let toggle = document.getElementById("toggle")
                if (toggle.style.display === "none") {
                    togle.style.display = "block";
                }
                else {
                    toggle.style.display = "none"
                }
                }*/

        }


        // If it is too far or too close from the original location, regenerate a new coordinates.
        else {
            console.log("New location is either too close or too far from the origin");
            this.error ++;
            if (this.error > 50) {
                Error("Oops an error has occured. Retry again!");
            }
            else {
                this.setRandomLocation();
                this.error = 0;
            }
        }

    }

    // THIS IS FOR THE ARRAY TO BE SHOWN IN A LIST
    /*savedArray(startDate, startLoc, endLoc, duration, distance) {
        return "Run date: " + startDate + "Start Location: " + startLoc + " End Location: " + endLoc + " Run time: " + duration + " Distance run: " + this.distance;
    }*/

    // Random value generator for the coordinates.
    randomGen() {
        // Setting the accuracy of the latitude and longitude to 0.001.
        return [(Math.random() * 2 - 1) * Math.random() *  0.001 + this.longitude, (Math.random() * 2 - 1) * Math.random() *  0.001 + this.latitude];

    }

}



// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

