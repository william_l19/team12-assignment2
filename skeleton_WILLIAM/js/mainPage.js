// Code for the main app page (Past Runs list).


//create list on index page
for (let i = 0; i < savedRuns.length; i++) {
    let li = document.createElement("li");
    li.setAttribute('class', 'mdl-list__item mdl-list__item--two-line');
    li.onclick = function(){viewRun(i)};

    let span1 = document.createElement("span");
    span1.setAttribute('class', 'mdl-list__item-primary-content');
    let span2 = document.createElement("span");
    let text = document.createTextNode(savedRuns[i].name);
    span2.appendChild(text);
    span1.appendChild(span2);
    li.appendChild((span1));
    document.getElementById("runsList").appendChild(li);
}
console.log(savedRuns.length);
console.log(typeof(savedRuns));
let li = document.createElement("li");

function viewRun(runIndex)
{
    localStorage.setItem("runIndex", JSON.stringify(runIndex));
    location.href = 'viewRun.html';
}
