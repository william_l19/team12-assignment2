// Code for the Measure Run page.

//Variables
let marker = 0, road = 0, timing, saveLoc, recTravels;
let temp, x;


document.getElementById("stop").disabled = true;
document.getElementById("start").disabled = true;
document.getElementById("save").disabled = true;

mapboxgl.accessToken = 'pk.eyJ1IjoibmljaG9sYXNuaWNob2xhcyIsImEiOiJjanpyd2hoZ3MxNm9hM25sbTkxc2Z3M3c0In0.0IOC3AapHLE9kYIBGwSbDQ';

let initLocation = JSON.parse(localStorage.getItem("current"));


let running = new Run(initLocation[0],initLocation[1], []);

let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    zoom: 16.5,
    center: initLocation

});
// The button for the map to zoom in or out.
map.addControl(new mapboxgl.NavigationControl());

// The button for the tracking of the current button.
map.addControl(new mapboxgl.GeolocateControl({
    positionOptions: {
        enableHighAccuracy: true,
        maxZoom: 20
    },
    trackUserLocation: true
}));

// Marker properties
let marker1 = new mapboxgl.Marker({
    color: "red"
});
let marker2 = new mapboxgl.Marker({
    color: "blue"
});



// Showing the marker
function showMarker() {
    if (marker === 0) {
        let markerData = JSON.parse(localStorage.getItem("markerKey"));
        marker2.setLngLat([markerData[0],markerData[1]]);
        marker2.addTo(map);
        marker2.className = "marker";
        marker++;
    }
    else {
        marker2.remove();
        marker--;
        showMarker()
    }

}



// Showing the path from current location to the random destination.
function showPath()
{
    if (road === 0) {
        let point = JSON.parse(localStorage.getItem("markerKey"));
        map.addLayer({
            "id": "route",
            "type": "line",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [
                            [point[0],point[1]],
                            initLocation
                        ]
                    }
                }
            },
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#888",
                "line-width": 7
            }
        });
        road++

    }
    else {
        map.removeLayer("route");
        map.removeSource("route");
        road--
        showPath()
    }
}

function showTravelPath(loc1, loc2, id) {
    map.on('load', function () {

        map.addLayer({
            "id": id,
            "type": "line",
            "source": {
                "type": "geojson",
                "data": {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                        "type": "LineString",
                        "coordinates": [
                            loc1, loc2
                        ]
                    }
                }
            },
            "layout": {
                "line-join": "round",
                "line-cap": "round"
            },
            "paint": {
                "line-color": "#308dff",
                "line-width": 5
            }
        });
    });
}

function showAllTravelPaths() {
    let tempData = JSON.parse(localStorage.getItem("current"));
    showTravelPath(temp, tempData, x.toString() );
    temp = JSON.parse(localStorage.getItem("current"));
    x++;
}

// Function for buttons
// To get the current location.
function buttonGetLocation() {
    alert(running.getLocation());
}

// To find a random location in the range of 60m to 150m.
function buttonSetRandomLocation() {
    //todo
    //set marker1
    marker1.setLngLat(JSON.parse(localStorage.getItem("current")));
    running.startLocation = JSON.parse(localStorage.getItem("current"));
    marker1.addTo(map);

    running.setRandomLocation();

    //set marker2 and show path betwwen marker1 & 2
    localStorage.setItem("markerKey", JSON.stringify(running.destination));
    showMarker();
    showPath();

    document.getElementById("stop").disabled = false;
    document.getElementById("start").disabled = false;
}


// Start time.
function startTimer() {
    running.runStartTime = new Date().getTime();
    timing = setInterval(counter, 1000);
    saveLoc = setInterval(saveLocation, 5000);
    document.getElementById("random").disabled = true;
    document.getElementById("start").disabled = true;

    temp = JSON.parse(localStorage.getItem("current"));
    console.log("111");
    console.log(temp);
    x = 1;
    recTravels = setInterval(showAllTravelPaths, 5000);


}

// Purpose is to stop the timer.
function stopTimer() {
    clearTimeout(timing);
    clearTimeout(saveLoc);
    clearTimeout(recTravels);
    document.getElementById("stop").disabled = true;
    document.getElementById("save").disabled = false;


}

// To calculate the duration of the run.
function counter() {
    running.runFinishTime= new Date().getTime();
    running.runDuration = Math.floor((running.runFinishTime - running.runStartTime) / 1000) ;
    //display time to html
    document.getElementById("time").innerHTML = running.runDuration + " Seconds";
}

function saveLocation() {
    let loc = JSON.parse(localStorage.getItem("current"));
    running.runTravelLocations.push(loc);
}


function saveRun() {

    let ask = prompt("Please enter the name of this run");

    while (ask === "") {
        ask = prompt("Please enter the name of this run");
    }

    running.name = ask;
    savedRuns.push(running);
    localStorage.setItem("saveRuns", JSON.stringify(savedRuns));


    document.getElementById("save").disabled = true;
    alert('You have successfully save the run');
    location.href = 'index.html';
}


